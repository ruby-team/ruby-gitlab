require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.pattern = FileList['./spec/**/*_spec.rb'] - FileList['./spec/gitlab/cli_spec.rb','./spec/gitlab/client/**']
end
